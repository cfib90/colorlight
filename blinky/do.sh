set -e
yosys -p 'read_verilog blinky.v; synth_ecp5; write_json blinky.json'
nextpnr-ecp5 --freq 25 --25k --package CABGA256 --speed 6 --json blinky.json --lpf blinky.lpf --write blinky-post-route.json --textcfg blinky.config
ecppack blinky.config blinky.bit
../tools/bit_to_svf.py blinky.bit blinky.svf
