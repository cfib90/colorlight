`default_nettype none
module blinky (input wire osc25m, output wire led);

    reg [23:0] clkdiv;
    reg led_oe;

    initial begin
        clkdiv <= 24'd6250000;
        led_oe <= 1'b1;
    end

    always @(posedge osc25m) begin
        if (clkdiv[23]) begin
            clkdiv <= 24'd6250000;
            led_oe <= ~led_oe;
        end else begin
            clkdiv <= clkdiv - 24'd1;
        end
    end

    assign led = (led_oe) ? 1'b1 : 1'b0;

endmodule
