`timescale 1ns/1ps
module tb();

    reg clk25m;
    wire led;

    initial begin
        $dumpvars();
        clk25m <= 1'b0;
        repeat (200) @(posedge clk25m);
        $finish;
    end

    always
        #20 clk25m <= ~clk25m;

    blinky dut(clk25m,led);

endmodule
