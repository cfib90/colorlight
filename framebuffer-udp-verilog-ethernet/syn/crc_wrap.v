module lfsr_wrap (
        input wire [7:0] crc_in,
        input wire [31:0] crc_state,
        output wire [31:0] crc_next);
        
    lfsr #(
        .LFSR_WIDTH(32),
        .LFSR_POLY(32'h4c11db7),
        .LFSR_CONFIG("GALOIS"),
        .LFSR_FEED_FORWARD(0),
        .REVERSE(1),
        .DATA_WIDTH(8),
        .STYLE("AUTO")
    )
    eth_crc_8 (
        .data_in(crc_in),
        .state_in(crc_state),
        .data_out(),
        .state_out(crc_next)
    );

endmodule
