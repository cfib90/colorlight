/*
 *  Copyright 2020, Christian Fibich
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose
 *  with or without fee is hereby granted, provided that the above copyright notice
 *  and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 *  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 *  AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 *  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 *  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 *  OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 *  PERFORMANCE OF THIS SOFTWARE.
 */

`default_nettype none
module top
    (
    input wire osc25m,
    /*
     * RGMII interface
     */
    input  wire                       rgmii_rx_clk,
    input  wire [3:0]                 rgmii_rxd,
    input  wire                       rgmii_rx_ctl,
    output wire                       rgmii_tx_clk,
    output wire [3:0]                 rgmii_txd,
    output wire                       rgmii_tx_ctl,
    /*
     * MDIO interface
     */
    output wire mdio_scl,
    output wire mdio_sda,
    /*
     * USER I/O (Button, LED)
     */
    input wire button,
    output wire led,
    output wire phy_resetn,
    
    output wire [3:0] R0,
    output wire [3:0] G0,
    output wire [3:0] B0,
    output wire [3:0] R1,
    output wire [3:0] G1,
    output wire [3:0] B1,
    output wire A,
    output wire B,
    output wire C,
    output wire D,
    output wire E, // a GND pin on the panels
    output wire LAT,
    output wire OE, //blank
    output wire CLK
    
);

    //------------------------------------------------------------------
    // PLL Instantiation and Locked Reset generation
    //------------------------------------------------------------------

    wire phy_init_done;
    wire                 locked;
    wire                 clock;
    wire                 clock90;
    reg [3:0]            locked_reset = 4'b1111;
    wire                 reset = locked_reset[3];
    wire                 display_clock;
    
    pll pll_inst(.clkin(osc25m),.clock(clock),.clock90(clock90),.panel_clock(display_clock),.locked(locked));
    
    always @(posedge clock or negedge locked) begin
        if (locked == 1'b0) begin
            locked_reset <= 4'b1111;
        end else begin
            locked_reset <= {locked_reset[2:0], 1'b0};
        end
    end
    
    reg [3:0] display_locked_reset = 4'b1111;
    wire display_reset = display_locked_reset[3];
    
    always @(posedge display_clock or negedge locked) begin
        if (locked == 1'b0) begin
            display_locked_reset <= 4'b1111;
        end else begin
            display_locked_reset <= {display_locked_reset[2:0], 1'b0};
        end
    end
    
    // Panel signals //
    
    wire [3:0]  ctrl_en;
    wire [3:0]  ctrl_wr;
    wire [15:0] ctrl_addr;
    wire [23:0] ctrl_wdat;

/**********************************************************************/

`define UDP

`ifdef UDP

    /* ---------------------------------------------------------------------- */
    /* Verilog-Ethernet */
    
    // AXI between MAC and Ethernet modules
    wire [7:0] rx_axis_tdata;
    wire rx_axis_tvalid;
    wire rx_axis_tready;
    wire rx_axis_tlast;
    wire rx_axis_tuser;

    wire [7:0] tx_axis_tdata;
    wire tx_axis_tvalid;
    wire tx_axis_tready;
    wire tx_axis_tlast;
    wire tx_axis_tuser;

    // Ethernet frame between Ethernet modules and UDP stack
    wire rx_eth_hdr_ready;
    wire rx_eth_hdr_valid;
    wire [47:0] rx_eth_dest_mac;
    wire [47:0] rx_eth_src_mac;
    wire [15:0] rx_eth_type;
    wire [7:0] rx_eth_payload_axis_tdata;
    wire rx_eth_payload_axis_tvalid;
    wire rx_eth_payload_axis_tready;
    wire rx_eth_payload_axis_tlast;
    wire rx_eth_payload_axis_tuser;

    wire tx_eth_hdr_ready;
    wire tx_eth_hdr_valid;
    wire [47:0] tx_eth_dest_mac;
    wire [47:0] tx_eth_src_mac;
    wire [15:0] tx_eth_type;
    wire [7:0] tx_eth_payload_axis_tdata;
    wire tx_eth_payload_axis_tvalid;
    wire tx_eth_payload_axis_tready;
    wire tx_eth_payload_axis_tlast;
    wire tx_eth_payload_axis_tuser;

    // IP frame connections
    wire rx_ip_hdr_valid;
    wire rx_ip_hdr_ready;
    wire [47:0] rx_ip_eth_dest_mac;
    wire [47:0] rx_ip_eth_src_mac;
    wire [15:0] rx_ip_eth_type;
    wire [3:0] rx_ip_version;
    wire [3:0] rx_ip_ihl;
    wire [5:0] rx_ip_dscp;
    wire [1:0] rx_ip_ecn;
    wire [15:0] rx_ip_length;
    wire [15:0] rx_ip_identification;
    wire [2:0] rx_ip_flags;
    wire [12:0] rx_ip_fragment_offset;
    wire [7:0] rx_ip_ttl;
    wire [7:0] rx_ip_protocol;
    wire [15:0] rx_ip_header_checksum;
    wire [31:0] rx_ip_source_ip;
    wire [31:0] rx_ip_dest_ip;
    wire [7:0] rx_ip_payload_axis_tdata;
    wire rx_ip_payload_axis_tvalid;
    wire rx_ip_payload_axis_tready;
    wire rx_ip_payload_axis_tlast;
    wire rx_ip_payload_axis_tuser;

    wire tx_ip_hdr_valid;
    wire tx_ip_hdr_ready;
    wire [5:0] tx_ip_dscp;
    wire [1:0] tx_ip_ecn;
    wire [15:0] tx_ip_length;
    wire [7:0] tx_ip_ttl;
    wire [7:0] tx_ip_protocol;
    wire [31:0] tx_ip_source_ip;
    wire [31:0] tx_ip_dest_ip;
    wire [7:0] tx_ip_payload_axis_tdata;
    wire tx_ip_payload_axis_tvalid;
    wire tx_ip_payload_axis_tready;
    wire tx_ip_payload_axis_tlast;
    wire tx_ip_payload_axis_tuser;

    // UDP frame connections
    wire rx_udp_hdr_valid;
    wire rx_udp_hdr_ready;
    wire [47:0] rx_udp_eth_dest_mac;
    wire [47:0] rx_udp_eth_src_mac;
    wire [15:0] rx_udp_eth_type;
    wire [3:0] rx_udp_ip_version;
    wire [3:0] rx_udp_ip_ihl;
    wire [5:0] rx_udp_ip_dscp;
    wire [1:0] rx_udp_ip_ecn;
    wire [15:0] rx_udp_ip_length;
    wire [15:0] rx_udp_ip_identification;
    wire [2:0] rx_udp_ip_flags;
    wire [12:0] rx_udp_ip_fragment_offset;
    wire [7:0] rx_udp_ip_ttl;
    wire [7:0] rx_udp_ip_protocol;
    wire [15:0] rx_udp_ip_header_checksum;
    wire [31:0] rx_udp_ip_source_ip;
    wire [31:0] rx_udp_ip_dest_ip;
    wire [15:0] rx_udp_source_port;
    wire [15:0] rx_udp_dest_port;
    wire [15:0] rx_udp_length;
    wire [15:0] rx_udp_checksum;
    wire [7:0] rx_udp_payload_axis_tdata;
    wire rx_udp_payload_axis_tvalid;
    wire rx_udp_payload_axis_tready;
    wire rx_udp_payload_axis_tlast;
    wire rx_udp_payload_axis_tuser;

    wire tx_udp_hdr_valid;
    wire tx_udp_hdr_ready;
    wire [5:0] tx_udp_ip_dscp;
    wire [1:0] tx_udp_ip_ecn;
    wire [7:0] tx_udp_ip_ttl;
    wire [31:0] tx_udp_ip_source_ip;
    wire [31:0] tx_udp_ip_dest_ip;
    wire [15:0] tx_udp_source_port;
    wire [15:0] tx_udp_dest_port;
    wire [15:0] tx_udp_length;
    wire [15:0] tx_udp_checksum;
    wire [7:0] tx_udp_payload_axis_tdata;
    wire tx_udp_payload_axis_tvalid;
    wire tx_udp_payload_axis_tready;
    wire tx_udp_payload_axis_tlast;
    wire tx_udp_payload_axis_tuser;

    wire [7:0] rx_fifo_udp_payload_axis_tdata;
    wire rx_fifo_udp_payload_axis_tvalid;
    wire rx_fifo_udp_payload_axis_tready;
    wire rx_fifo_udp_payload_axis_tlast;
    wire rx_fifo_udp_payload_axis_tuser;

    wire [7:0] tx_fifo_udp_payload_axis_tdata;
    wire tx_fifo_udp_payload_axis_tvalid;
    wire tx_fifo_udp_payload_axis_tready;
    wire tx_fifo_udp_payload_axis_tlast;
    wire tx_fifo_udp_payload_axis_tuser;

    // Configuration
    wire [47:0] local_mac   = 48'h02_00_00_00_00_00;
    wire [31:0] local_ip    = {8'd192, 8'd168, 8'd1,   8'd50};
    wire [31:0] gateway_ip  = {8'd192, 8'd168, 8'd1,   8'd1};
    wire [31:0] subnet_mask = {8'd255, 8'd255, 8'd255, 8'd0};

    // IP ports not used
    assign rx_ip_hdr_ready = 1;
    assign rx_ip_payload_axis_tready = 1;

    assign tx_ip_hdr_valid = 0;
    assign tx_ip_dscp = 0;
    assign tx_ip_ecn = 0;
    assign tx_ip_length = 0;
    assign tx_ip_ttl = 0;
    assign tx_ip_protocol = 0;
    assign tx_ip_source_ip = 0;
    assign tx_ip_dest_ip = 0;
    assign tx_ip_payload_axis_tdata = 0;
    assign tx_ip_payload_axis_tvalid = 0;
    assign tx_ip_payload_axis_tlast = 0;
    assign tx_ip_payload_axis_tuser = 0;

    // Loop back UDP
    wire match_cond = rx_udp_dest_port == 1234;
    wire no_match = !match_cond;

    reg match_cond_reg = 0;
    reg no_match_reg = 0;

    always @(posedge display_clock) begin
        if (reset) begin
            match_cond_reg <= 0;
            no_match_reg <= 0;
        end else begin
            if (rx_udp_payload_axis_tvalid) begin
                if ((!match_cond_reg && !no_match_reg) ||
                    (rx_udp_payload_axis_tvalid && rx_udp_payload_axis_tready && rx_udp_payload_axis_tlast)) begin
                    match_cond_reg <= match_cond;
                    no_match_reg <= no_match;
                end
            end else begin
                match_cond_reg <= 0;
                no_match_reg <= 0;
            end
        end
    end

    assign tx_udp_hdr_valid = rx_udp_hdr_valid && match_cond;
    assign rx_udp_hdr_ready = (tx_eth_hdr_ready && match_cond) || no_match;
    assign tx_udp_ip_dscp = 0;
    assign tx_udp_ip_ecn = 0;
    assign tx_udp_ip_ttl = 64;
    assign tx_udp_ip_source_ip = local_ip;
    assign tx_udp_ip_dest_ip = rx_udp_ip_source_ip;
    assign tx_udp_source_port = rx_udp_dest_port;
    assign tx_udp_dest_port = rx_udp_source_port;
    assign tx_udp_length = rx_udp_length;
    assign tx_udp_checksum = 0;

    assign tx_udp_payload_axis_tdata = tx_fifo_udp_payload_axis_tdata;
    assign tx_udp_payload_axis_tvalid = tx_fifo_udp_payload_axis_tvalid;
    assign tx_fifo_udp_payload_axis_tready = tx_udp_payload_axis_tready;
    assign tx_udp_payload_axis_tlast = tx_fifo_udp_payload_axis_tlast;
    assign tx_udp_payload_axis_tuser = tx_fifo_udp_payload_axis_tuser;

    assign rx_fifo_udp_payload_axis_tdata = rx_udp_payload_axis_tdata;
    assign rx_fifo_udp_payload_axis_tvalid = rx_udp_payload_axis_tvalid && match_cond_reg;
    assign rx_udp_payload_axis_tready = (rx_fifo_udp_payload_axis_tready && match_cond_reg) || no_match_reg;
    assign rx_fifo_udp_payload_axis_tlast = rx_udp_payload_axis_tlast;
    assign rx_fifo_udp_payload_axis_tuser = rx_udp_payload_axis_tuser;



    // Place first payload byte onto LEDs
    reg valid_last = 0;
    reg [7:0] led_reg = 0;

    always @(posedge clock) begin
        if (reset) begin
            led_reg <= 0;
        end else begin
            if (tx_udp_payload_axis_tvalid) begin
                if (!valid_last) begin
                    led_reg <= tx_udp_payload_axis_tdata;
                    valid_last <= 1'b1;
                end
                if (tx_udp_payload_axis_tlast) begin
                    valid_last <= 1'b0;
                end
            end
        end
    end

    wire gf;
    reg gf_d;

    always @(posedge display_clock) begin
        if (reset) begin
            gf_d <= 1'b0;
        end else begin
            gf_d <= gf_d | gf;
        end
    end

    //assign led = sw;
    //assign led = ~gf_d;
    //assign phy_reset_n = !reset;
    
    reg [24:0] cntr = 25'b0;
    
    always @(posedge rgmii_rx_clk) begin
        cntr <= cntr + 1;
    end
    

    eth_mac_1g_rgmii_fifo #(
        .TARGET("ECP5"),
        .IODDR_STYLE("IODDR"),
        .CLOCK_INPUT_STYLE("BUFR"),
        .USE_CLK90("FALSE"),
        .ENABLE_PADDING(1),
        .MIN_FRAME_LENGTH(64),
        .TX_FIFO_DEPTH(4096),
        .TX_FRAME_FIFO(1),
        .RX_FIFO_DEPTH(4096),
        .RX_FRAME_FIFO(1)
    )
    eth_mac_inst (
        .gtx_clk(clock),
        .gtx_clk90(clock90),
        .gtx_rst(reset),
        .logic_clk(display_clock),
        .logic_rst(display_reset),

        .tx_axis_tdata(tx_axis_tdata),
        .tx_axis_tvalid(tx_axis_tvalid),
        .tx_axis_tready(tx_axis_tready),
        .tx_axis_tlast(tx_axis_tlast),
        .tx_axis_tuser(tx_axis_tuser),

        .rx_axis_tdata(rx_axis_tdata),
        .rx_axis_tvalid(rx_axis_tvalid),
        .rx_axis_tready(rx_axis_tready),
        .rx_axis_tlast(rx_axis_tlast),
        .rx_axis_tuser(rx_axis_tuser),

        .rgmii_rx_clk(rgmii_rx_clk),
        .rgmii_rxd(rgmii_rxd),
        .rgmii_rx_ctl(rgmii_rx_ctl),
        .rgmii_tx_clk(rgmii_tx_clk),
        .rgmii_txd(rgmii_txd),
        .rgmii_tx_ctl(rgmii_tx_ctl),

        .tx_fifo_overflow(),
        .tx_fifo_bad_frame(),
        .tx_fifo_good_frame(),
        .rx_error_bad_frame(),
        .rx_error_bad_fcs(),
        .rx_fifo_overflow(),
        .rx_fifo_bad_frame(),
        .rx_fifo_good_frame(gf),
        .speed(),

        .ifg_delay(12)
    );

    eth_axis_rx
    eth_axis_rx_inst (
        .clk(display_clock),
        .rst(display_reset),
        // AXI input
        .s_axis_tdata(rx_axis_tdata),
        .s_axis_tvalid(rx_axis_tvalid),
        .s_axis_tready(rx_axis_tready),
        .s_axis_tlast(rx_axis_tlast),
        .s_axis_tuser(rx_axis_tuser),
        // Ethernet frame output
        .m_eth_hdr_valid(rx_eth_hdr_valid),
        .m_eth_hdr_ready(rx_eth_hdr_ready),
        .m_eth_dest_mac(rx_eth_dest_mac),
        .m_eth_src_mac(rx_eth_src_mac),
        .m_eth_type(rx_eth_type),
        .m_eth_payload_axis_tdata(rx_eth_payload_axis_tdata),
        .m_eth_payload_axis_tvalid(rx_eth_payload_axis_tvalid),
        .m_eth_payload_axis_tready(rx_eth_payload_axis_tready),
        .m_eth_payload_axis_tlast(rx_eth_payload_axis_tlast),
        .m_eth_payload_axis_tuser(rx_eth_payload_axis_tuser),
        // Status signals
        .busy(),
        .error_header_early_termination()
    );
    
    reg [10:0] pre;
    reg [4:0]  bitcnt = 0;
    reg uart       = 1'b1;
    reg [7:0] sr;
    
    wire start = rx_axis_tvalid && rx_axis_tready; //rx_start_packet | rx_error_bad_frame | rx_error_bad_fcs; //rx_axis_tvalid & ~rx_axis_tvalid_dly;
    
    reg [7:0] data;
    reg started;
    wire [7:0] uart_data = rx_axis_tdata; //rx_axis_tdata;
    
    //reg rx_axis_tvalid_dly;
    
    always @(posedge display_clock) begin
        pre     <= pre + 1;
        started <= started | start;
        //rx_axis_tvalid_dly <= rx_axis_tvalid;
        if (start) begin
            data <= uart_data;
        end
        if (pre == 217) begin
            pre <= 0;
            if (bitcnt == 0) begin
                if (started) begin
                    {sr,uart} <= {data,1'b0};
                    bitcnt       <= bitcnt + 1;
                    started   <= 1'b0;
                end
            end else begin
                {sr,uart} <= {1'b1,sr};
                bitcnt       <= bitcnt + 1;
            end
            if (bitcnt == 24) begin
                bitcnt <= 0;
            end
        end
    end
    
    assign led = uart;

    eth_axis_tx
    eth_axis_tx_inst (
        .clk(display_clock),
        .rst(display_reset),
        // Ethernet frame input
        .s_eth_hdr_valid(tx_eth_hdr_valid),
        .s_eth_hdr_ready(tx_eth_hdr_ready),
        .s_eth_dest_mac(tx_eth_dest_mac),
        .s_eth_src_mac(tx_eth_src_mac),
        .s_eth_type(tx_eth_type),
        .s_eth_payload_axis_tdata(tx_eth_payload_axis_tdata),
        .s_eth_payload_axis_tvalid(tx_eth_payload_axis_tvalid),
        .s_eth_payload_axis_tready(tx_eth_payload_axis_tready),
        .s_eth_payload_axis_tlast(tx_eth_payload_axis_tlast),
        .s_eth_payload_axis_tuser(tx_eth_payload_axis_tuser),
        // AXI output
        .m_axis_tdata(tx_axis_tdata),
        .m_axis_tvalid(tx_axis_tvalid),
        .m_axis_tready(tx_axis_tready),
        .m_axis_tlast(tx_axis_tlast),
        .m_axis_tuser(tx_axis_tuser),
        // Status signals
        .busy()
    );

    udp_complete #(
        .ARP_CACHE_ADDR_WIDTH(2),
    )
    udp_complete_inst (
        .clk(display_clock),
        .rst(display_reset),
        // Ethernet frame input
        .s_eth_hdr_valid(rx_eth_hdr_valid),
        .s_eth_hdr_ready(rx_eth_hdr_ready),
        .s_eth_dest_mac(rx_eth_dest_mac),
        .s_eth_src_mac(rx_eth_src_mac),
        .s_eth_type(rx_eth_type),
        .s_eth_payload_axis_tdata(rx_eth_payload_axis_tdata),
        .s_eth_payload_axis_tvalid(rx_eth_payload_axis_tvalid),
        .s_eth_payload_axis_tready(rx_eth_payload_axis_tready),
        .s_eth_payload_axis_tlast(rx_eth_payload_axis_tlast),
        .s_eth_payload_axis_tuser(rx_eth_payload_axis_tuser),
        // Ethernet frame output
        .m_eth_hdr_valid(tx_eth_hdr_valid),
        .m_eth_hdr_ready(tx_eth_hdr_ready),
        .m_eth_dest_mac(tx_eth_dest_mac),
        .m_eth_src_mac(tx_eth_src_mac),
        .m_eth_type(tx_eth_type),
        .m_eth_payload_axis_tdata(tx_eth_payload_axis_tdata),
        .m_eth_payload_axis_tvalid(tx_eth_payload_axis_tvalid),
        .m_eth_payload_axis_tready(tx_eth_payload_axis_tready),
        .m_eth_payload_axis_tlast(tx_eth_payload_axis_tlast),
        .m_eth_payload_axis_tuser(tx_eth_payload_axis_tuser),
        // IP frame input
        .s_ip_hdr_valid(tx_ip_hdr_valid),
        .s_ip_hdr_ready(tx_ip_hdr_ready),
        .s_ip_dscp(tx_ip_dscp),
        .s_ip_ecn(tx_ip_ecn),
        .s_ip_length(tx_ip_length),
        .s_ip_ttl(tx_ip_ttl),
        .s_ip_protocol(tx_ip_protocol),
        .s_ip_source_ip(tx_ip_source_ip),
        .s_ip_dest_ip(tx_ip_dest_ip),
        .s_ip_payload_axis_tdata(tx_ip_payload_axis_tdata),
        .s_ip_payload_axis_tvalid(tx_ip_payload_axis_tvalid),
        .s_ip_payload_axis_tready(tx_ip_payload_axis_tready),
        .s_ip_payload_axis_tlast(tx_ip_payload_axis_tlast),
        .s_ip_payload_axis_tuser(tx_ip_payload_axis_tuser),
        // IP frame output
        .m_ip_hdr_valid(rx_ip_hdr_valid),
        .m_ip_hdr_ready(rx_ip_hdr_ready),
        .m_ip_eth_dest_mac(rx_ip_eth_dest_mac),
        .m_ip_eth_src_mac(rx_ip_eth_src_mac),
        .m_ip_eth_type(rx_ip_eth_type),
        .m_ip_version(rx_ip_version),
        .m_ip_ihl(rx_ip_ihl),
        .m_ip_dscp(rx_ip_dscp),
        .m_ip_ecn(rx_ip_ecn),
        .m_ip_length(rx_ip_length),
        .m_ip_identification(rx_ip_identification),
        .m_ip_flags(rx_ip_flags),
        .m_ip_fragment_offset(rx_ip_fragment_offset),
        .m_ip_ttl(rx_ip_ttl),
        .m_ip_protocol(rx_ip_protocol),
        .m_ip_header_checksum(rx_ip_header_checksum),
        .m_ip_source_ip(rx_ip_source_ip),
        .m_ip_dest_ip(rx_ip_dest_ip),
        .m_ip_payload_axis_tdata(rx_ip_payload_axis_tdata),
        .m_ip_payload_axis_tvalid(rx_ip_payload_axis_tvalid),
        .m_ip_payload_axis_tready(rx_ip_payload_axis_tready),
        .m_ip_payload_axis_tlast(rx_ip_payload_axis_tlast),
        .m_ip_payload_axis_tuser(rx_ip_payload_axis_tuser),
        // UDP frame input
        .s_udp_hdr_valid(tx_udp_hdr_valid),
        .s_udp_hdr_ready(tx_udp_hdr_ready),
        .s_udp_ip_dscp(tx_udp_ip_dscp),
        .s_udp_ip_ecn(tx_udp_ip_ecn),
        .s_udp_ip_ttl(tx_udp_ip_ttl),
        .s_udp_ip_source_ip(tx_udp_ip_source_ip),
        .s_udp_ip_dest_ip(tx_udp_ip_dest_ip),
        .s_udp_source_port(tx_udp_source_port),
        .s_udp_dest_port(tx_udp_dest_port),
        .s_udp_length(tx_udp_length),
        .s_udp_checksum(tx_udp_checksum),
        .s_udp_payload_axis_tdata(tx_udp_payload_axis_tdata),
        .s_udp_payload_axis_tvalid(tx_udp_payload_axis_tvalid),
        .s_udp_payload_axis_tready(tx_udp_payload_axis_tready),
        .s_udp_payload_axis_tlast(tx_udp_payload_axis_tlast),
        .s_udp_payload_axis_tuser(tx_udp_payload_axis_tuser),
        // UDP frame output
        .m_udp_hdr_valid(rx_udp_hdr_valid),
        .m_udp_hdr_ready(rx_udp_hdr_ready),
        .m_udp_eth_dest_mac(rx_udp_eth_dest_mac),
        .m_udp_eth_src_mac(rx_udp_eth_src_mac),
        .m_udp_eth_type(rx_udp_eth_type),
        .m_udp_ip_version(rx_udp_ip_version),
        .m_udp_ip_ihl(rx_udp_ip_ihl),
        .m_udp_ip_dscp(rx_udp_ip_dscp),
        .m_udp_ip_ecn(rx_udp_ip_ecn),
        .m_udp_ip_length(rx_udp_ip_length),
        .m_udp_ip_identification(rx_udp_ip_identification),
        .m_udp_ip_flags(rx_udp_ip_flags),
        .m_udp_ip_fragment_offset(rx_udp_ip_fragment_offset),
        .m_udp_ip_ttl(rx_udp_ip_ttl),
        .m_udp_ip_protocol(rx_udp_ip_protocol),
        .m_udp_ip_header_checksum(rx_udp_ip_header_checksum),
        .m_udp_ip_source_ip(rx_udp_ip_source_ip),
        .m_udp_ip_dest_ip(rx_udp_ip_dest_ip),
        .m_udp_source_port(rx_udp_source_port),
        .m_udp_dest_port(rx_udp_dest_port),
        .m_udp_length(rx_udp_length),
        .m_udp_checksum(rx_udp_checksum),
        .m_udp_payload_axis_tdata(rx_udp_payload_axis_tdata),
        .m_udp_payload_axis_tvalid(rx_udp_payload_axis_tvalid),
        .m_udp_payload_axis_tready(rx_udp_payload_axis_tready),
        .m_udp_payload_axis_tlast(rx_udp_payload_axis_tlast),
        .m_udp_payload_axis_tuser(rx_udp_payload_axis_tuser),
        // Status signals
        .ip_rx_busy(),
        .ip_tx_busy(),
        .udp_rx_busy(),
        .udp_tx_busy(),
        .ip_rx_error_header_early_termination(),
        .ip_rx_error_payload_early_termination(),
        .ip_rx_error_invalid_header(),
        .ip_rx_error_invalid_checksum(),
        .ip_tx_error_payload_early_termination(),
        .ip_tx_error_arp_failed(),
        .udp_rx_error_header_early_termination(),
        .udp_rx_error_payload_early_termination(),
        .udp_tx_error_payload_early_termination(),
        // Configuration
        .local_mac(local_mac),
        .local_ip(local_ip),
        .gateway_ip(gateway_ip),
        .subnet_mask(subnet_mask),
        .clear_arp_cache(0)
    );

    axis_fifo #(
        .DEPTH(8192),
        .DATA_WIDTH(8),
        .KEEP_ENABLE(0),
        .ID_ENABLE(0),
        .DEST_ENABLE(0),
        .USER_ENABLE(1),
        .USER_WIDTH(1),
        .FRAME_FIFO(0)
    )
    udp_payload_fifo (
        .clk(display_clock),
        .rst(display_reset),

        // AXI input
        .s_axis_tdata(rx_fifo_udp_payload_axis_tdata),
        .s_axis_tkeep(0),
        .s_axis_tvalid(rx_fifo_udp_payload_axis_tvalid),
        .s_axis_tready(rx_fifo_udp_payload_axis_tready),
        .s_axis_tlast(rx_fifo_udp_payload_axis_tlast),
        .s_axis_tid(0),
        .s_axis_tdest(0),
        .s_axis_tuser(rx_fifo_udp_payload_axis_tuser),

        // AXI output
        .m_axis_tdata(tx_fifo_udp_payload_axis_tdata),
        .m_axis_tkeep(),
        .m_axis_tvalid(tx_fifo_udp_payload_axis_tvalid),
        .m_axis_tready(tx_fifo_udp_payload_axis_tready),
        .m_axis_tlast(tx_fifo_udp_payload_axis_tlast),
        .m_axis_tid(),
        .m_axis_tdest(),
        .m_axis_tuser(tx_fifo_udp_payload_axis_tuser),

        // Status
        .status_overflow(),
        .status_bad_frame(),
        .status_good_frame()
    );


    /* ----------------------------------------------------------------------- */

    wire          udp_sink_valid       = 1'b0;
    wire          udp_sink_last        = 1'b0;
    wire          udp_sink_ready       ;
    wire  [15:0]  udp_sink_src_port    = 16'b0;
    wire  [15:0]  udp_sink_dst_port    = 16'b0;
    wire  [31:0]  udp_sink_ip_address  = 32'b0;
    wire  [15:0]  udp_sink_length      = 16'b0;
    wire  [31:0]  udp_sink_data        = 32'b0;
    wire  [3:0]   udp_sink_error       = 4'b0;
    wire          udp_source_valid     ;
    wire          udp_source_last      ;
    wire          udp_source_ready     ;
    wire  [15:0]  udp_source_src_port  ;
    wire  [15:0]  udp_source_dst_port  ;
    wire  [31:0]  udp_source_ip_address;
    wire  [15:0]  udp_source_length    ;
    wire  [31:0]  udp_source_data      ;
    wire  [3:0]   udp_source_error     ;
    
    `ifdef 0
    phy_sequencer phy_sequencer_inst (.clock(display_clock),
                  .reset(reset),
                  .phy_resetn(phy_resetn),
                  .mdio_scl(mdio_scl),
                  .mdio_sda(mdio_sda),
                  .phy_init_done(phy_init_done));
    `endif
    
    assign mdio_scl = 1'b1;
    assign mdio_sda = 1'b1;
    
    wire [3:0]  ctrl_en;
    wire [3:0]  ctrl_wr;
    wire [15:0] ctrl_addr;
    wire [23:0] ctrl_wdat;

    udp_panel_writer udp_inst 
                    (.clock(display_clock),
                     .reset(display_reset),
                     
                     .udp_source_valid(rx_udp_hdr_valid),
                     .udp_source_last(udp_source_last),
                     .udp_source_ready(udp_source_ready),
                     .udp_source_src_port(rx_udp_source_port),
                     .udp_source_dst_port(tx_udp_source_port),
                     .udp_source_ip_address(rx_udp_ip_source_ip),
                     .udp_source_length(rx_udp_ip_length),
                     .udp_source_data(rx_udp_payload_axis_tdata),
                     .udp_source_error(4'b0),

                     .ctrl_en(ctrl_en),
                     .ctrl_wr(ctrl_wr),
                     .ctrl_addr(ctrl_addr),
                     .ctrl_wdat(ctrl_wdat),
                     .led_reg()
                     );

    `endif

    genvar panel_index;
    
    wire [3:0] A_int;
    wire [3:0] B_int;
    wire [3:0] C_int;
    wire [3:0] D_int;
    wire [3:0] LAT_int;
    wire [3:0] OE_int;
    wire [3:0] CLK_int;

    generate
        for (panel_index = 0; panel_index < 4; panel_index=panel_index+1) begin

            mod_panel panel_inst (
                .ctrl_clk(display_clock),
                .ctrl_en(ctrl_en[panel_index]),
                .ctrl_wr(ctrl_wr),       // Which color memory block to write
                .ctrl_addr(ctrl_addr),   // Addr to write color info on [col_info][row_info]
                .ctrl_wdat(ctrl_wdat),   // Data to be written [R][G][B]

                .display_clock(display_clock),
                .panel_r0(R0[panel_index]),
                .panel_g0(G0[panel_index]),
                .panel_b0(B0[panel_index]),
                .panel_r1(R1[panel_index]),
                .panel_g1(G1[panel_index]),
                .panel_b1(B1[panel_index]),
                .panel_a(A_int[panel_index]),
                .panel_b(B_int[panel_index]),
                .panel_c(C_int[panel_index]),
                .panel_d(D_int[panel_index]),
                .panel_clk(CLK_int[panel_index]),
                .panel_stb(LAT_int[panel_index]),
                .panel_oe(OE_int[panel_index])
            );
        end
    endgenerate
    
    assign A = A_int[0];
    assign B = B_int[0];
    assign C = C_int[0];
    assign D = D_int[0];
    assign LAT = LAT_int[0];
    assign OE  = OE_int[0];
    assign CLK = CLK_int[0];
    assign E = 1'b0;
endmodule

module lfsr #
(
    // width of LFSR
    parameter LFSR_WIDTH = 31,
    // LFSR polynomial
    parameter LFSR_POLY = 31'h10000001,
    // LFSR configuration: "GALOIS", "FIBONACCI"
    parameter LFSR_CONFIG = "FIBONACCI",
    // LFSR feed forward enable
    parameter LFSR_FEED_FORWARD = 0,
    // bit-reverse input and output
    parameter REVERSE = 0,
    // width of data input
    parameter DATA_WIDTH = 8,
    // implementation style: "AUTO", "LOOP", "REDUCTION"
    parameter STYLE = "AUTO"
)
(
    input  wire [DATA_WIDTH-1:0] data_in,
    input  wire [LFSR_WIDTH-1:0] state_in,
    output wire [DATA_WIDTH-1:0] data_out,
    output wire [LFSR_WIDTH-1:0] state_out
); 

    initial begin
        if (LFSR_WIDTH != 32 ||
            LFSR_POLY  != 32'h4c11db7 ||
            LFSR_CONFIG != "GALOIS" ||
            LFSR_FEED_FORWARD != 0 ||
            REVERSE != 1 ||
            DATA_WIDTH != 8 ||
            STYLE != "AUTO") begin
            //$fatal("INVALID LFSR CONFIG");
        end
    end
    
    eth_crc_8 crc (
        .data_in(data_in),
        .crc_state(state_in),
        .crc_next(state_out)
    );


endmodule

module eth_crc_8 (
    input  wire [7:0] data_in,
    input  wire [31:0] crc_state,
    output wire [31:0] crc_next
);

assign crc_next[0] = crc_state[2] ^ crc_state[8] ^ data_in[2];
assign crc_next[1] = crc_state[0] ^ crc_state[3] ^ crc_state[9] ^ data_in[0] ^ data_in[3];
assign crc_next[2] = crc_state[0] ^ crc_state[1] ^ crc_state[4] ^ crc_state[10] ^ data_in[0] ^ data_in[1] ^ data_in[4];
assign crc_next[3] = crc_state[1] ^ crc_state[2] ^ crc_state[5] ^ crc_state[11] ^ data_in[1] ^ data_in[2] ^ data_in[5];
assign crc_next[4] = crc_state[0] ^ crc_state[2] ^ crc_state[3] ^ crc_state[6] ^ crc_state[12] ^ data_in[0] ^ data_in[2] ^ data_in[3] ^ data_in[6];
assign crc_next[5] = crc_state[1] ^ crc_state[3] ^ crc_state[4] ^ crc_state[7] ^ crc_state[13] ^ data_in[1] ^ data_in[3] ^ data_in[4] ^ data_in[7];
assign crc_next[6] = crc_state[4] ^ crc_state[5] ^ crc_state[14] ^ data_in[4] ^ data_in[5];
assign crc_next[7] = crc_state[0] ^ crc_state[5] ^ crc_state[6] ^ crc_state[15] ^ data_in[0] ^ data_in[5] ^ data_in[6];
assign crc_next[8] = crc_state[1] ^ crc_state[6] ^ crc_state[7] ^ crc_state[16] ^ data_in[1] ^ data_in[6] ^ data_in[7];
assign crc_next[9] = crc_state[7] ^ crc_state[17] ^ data_in[7];
assign crc_next[10] = crc_state[2] ^ crc_state[18] ^ data_in[2];
assign crc_next[11] = crc_state[3] ^ crc_state[19] ^ data_in[3];
assign crc_next[12] = crc_state[0] ^ crc_state[4] ^ crc_state[20] ^ data_in[0] ^ data_in[4];
assign crc_next[13] = crc_state[0] ^ crc_state[1] ^ crc_state[5] ^ crc_state[21] ^ data_in[0] ^ data_in[1] ^ data_in[5];
assign crc_next[14] = crc_state[1] ^ crc_state[2] ^ crc_state[6] ^ crc_state[22] ^ data_in[1] ^ data_in[2] ^ data_in[6];
assign crc_next[15] = crc_state[2] ^ crc_state[3] ^ crc_state[7] ^ crc_state[23] ^ data_in[2] ^ data_in[3] ^ data_in[7];
assign crc_next[16] = crc_state[0] ^ crc_state[2] ^ crc_state[3] ^ crc_state[4] ^ crc_state[24] ^ data_in[0] ^ data_in[2] ^ data_in[3] ^ data_in[4];
assign crc_next[17] = crc_state[0] ^ crc_state[1] ^ crc_state[3] ^ crc_state[4] ^ crc_state[5] ^ crc_state[25] ^ data_in[0] ^ data_in[1] ^ data_in[3] ^ data_in[4] ^ data_in[5];
assign crc_next[18] = crc_state[0] ^ crc_state[1] ^ crc_state[2] ^ crc_state[4] ^ crc_state[5] ^ crc_state[6] ^ crc_state[26] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[4] ^ data_in[5] ^ data_in[6];
assign crc_next[19] = crc_state[1] ^ crc_state[2] ^ crc_state[3] ^ crc_state[5] ^ crc_state[6] ^ crc_state[7] ^ crc_state[27] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[5] ^ data_in[6] ^ data_in[7];
assign crc_next[20] = crc_state[3] ^ crc_state[4] ^ crc_state[6] ^ crc_state[7] ^ crc_state[28] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[7];
assign crc_next[21] = crc_state[2] ^ crc_state[4] ^ crc_state[5] ^ crc_state[7] ^ crc_state[29] ^ data_in[2] ^ data_in[4] ^ data_in[5] ^ data_in[7];
assign crc_next[22] = crc_state[2] ^ crc_state[3] ^ crc_state[5] ^ crc_state[6] ^ crc_state[30] ^ data_in[2] ^ data_in[3] ^ data_in[5] ^ data_in[6];
assign crc_next[23] = crc_state[3] ^ crc_state[4] ^ crc_state[6] ^ crc_state[7] ^ crc_state[31] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[7];
assign crc_next[24] = crc_state[0] ^ crc_state[2] ^ crc_state[4] ^ crc_state[5] ^ crc_state[7] ^ data_in[0] ^ data_in[2] ^ data_in[4] ^ data_in[5] ^ data_in[7];
assign crc_next[25] = crc_state[0] ^ crc_state[1] ^ crc_state[2] ^ crc_state[3] ^ crc_state[5] ^ crc_state[6] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[5] ^ data_in[6];
assign crc_next[26] = crc_state[0] ^ crc_state[1] ^ crc_state[2] ^ crc_state[3] ^ crc_state[4] ^ crc_state[6] ^ crc_state[7] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[7];
assign crc_next[27] = crc_state[1] ^ crc_state[3] ^ crc_state[4] ^ crc_state[5] ^ crc_state[7] ^ data_in[1] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[7];
assign crc_next[28] = crc_state[0] ^ crc_state[4] ^ crc_state[5] ^ crc_state[6] ^ data_in[0] ^ data_in[4] ^ data_in[5] ^ data_in[6];
assign crc_next[29] = crc_state[0] ^ crc_state[1] ^ crc_state[5] ^ crc_state[6] ^ crc_state[7] ^ data_in[0] ^ data_in[1] ^ data_in[5] ^ data_in[6] ^ data_in[7];
assign crc_next[30] = crc_state[0] ^ crc_state[1] ^ crc_state[6] ^ crc_state[7] ^ data_in[0] ^ data_in[1] ^ data_in[6] ^ data_in[7];
assign crc_next[31] = crc_state[1] ^ crc_state[7] ^ data_in[1] ^ data_in[7];

endmodule
