set -e
yosys -p 'read_verilog ../blinky/blinky.v; read_verilog mod_panel.v; read_verilog mod_top.v; synth_ecp5; write_json framebuffer.json'
nextpnr-ecp5 --freq 25 --25k --package CABGA256 --speed 6 --json framebuffer.json --lpf framebuffer.lpf --write framebuffer-post-route.json --textcfg framebuffer.config
ecppack framebuffer.config framebuffer.bit
../tools/bit_to_svf.py framebuffer.bit framebuffer.svf
