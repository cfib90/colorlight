/*******************************************************************************
** Project    : LED MATRIX
********************************************************************************
** File       : mod_panel.v
** Author     : Kelve T. Henrique
** Company    : FH Technikum Wien
** Last update: 2019 Jan 09
** Platform   : Linux(Programming), ModelSim(Simul), Quartus II13.1(Synth)
********************************************************************************
** Description: Modified code for one panel from cloned code as specified 
**              in README
*******************************************************************************/

// Description of the LED panel:
// http://bikerglen.com/projects/lighting/led-panel-1up/#The_LED_Panel
//
// panel_[abcd] ... select rows (in pairs from top and bottom half)
// panel_oe ....... display the selected rows (active low)
// panel_clk ...... serial clock for color data
// panel_stb ...... latch shifted data (active high)
// panel_[rgb]0 ... color channel for top half
// panel_[rgb]1 ... color channel for bottom half

`define TESTBENCH
`default_nettype none
module mod_panel (
	input clk,
	input resetn,
	input ctrl_en,
	input [3:0] ctrl_wr,           // Which color memory block to write
	input [15:0] ctrl_addr,        // Addr to write color info on [col_info][row_info]
	input [23:0] ctrl_wdat,        // Data to be written [R][G][B]

	output reg ctrl_done,          // Assures data is written at least in altern. clks
	output reg panel_r0, panel_g0, panel_b0, panel_r1, panel_g1, panel_b1,
	output reg panel_a, panel_b, panel_c, panel_d, panel_clk, panel_stb, panel_oe
);

	parameter integer MAX_BITS_PER_CHANNEL = 8;       // i.e. ctrl_wdat should be 3*MAX_BITS_PER_CHANNEL wide
	parameter integer BITS_PER_CHANNEL     = 4;
	parameter integer CLOCK_FREQ_HZ        = 6000000;
	parameter integer SIZE                 = 3'd4;       // number of panels
	parameter integer EXTRA_BLANKING       = 0;

	localparam integer SIZE_BITS = $clog2(SIZE);    // ceiling of ln()

    /* HIGH 5-BITS: ROW(X) --- LOW 5-BITS: COLUMN(Y) */
	reg [BITS_PER_CHANNEL-1:0] video_mem_r [0:SIZE*1024-1];
	reg [BITS_PER_CHANNEL-1:0] video_mem_g [0:SIZE*1024-1];
	reg [BITS_PER_CHANNEL-1:0] video_mem_b [0:SIZE*1024-1];

`ifdef TESTBENCH
	initial begin:video_mem_init
		integer i;
        ctrl_done <= 1;
        $readmemh("red.mem",video_mem_r);
        $readmemh("green.mem",video_mem_g);
        $readmemh("blue.mem",video_mem_b);
	end
`endif

    /* FILLING MEMORY BLOCKS FOR RED, GREEN, BLUE CHANNELS */
	always @(posedge clk) begin
		ctrl_done <= (|ctrl_wr && ctrl_en);

		if (ctrl_done && ctrl_wr[2]) video_mem_r[ctrl_addr] <= ctrl_wdat[3*MAX_BITS_PER_CHANNEL-1:2*MAX_BITS_PER_CHANNEL+BITS_PER_CHANNEL];
		if (ctrl_done && ctrl_wr[1]) video_mem_g[ctrl_addr] <= ctrl_wdat[2*MAX_BITS_PER_CHANNEL-1:1*MAX_BITS_PER_CHANNEL+BITS_PER_CHANNEL];
		if (ctrl_done && ctrl_wr[0]) video_mem_b[ctrl_addr] <= ctrl_wdat[1*MAX_BITS_PER_CHANNEL-1:0*MAX_BITS_PER_CHANNEL+BITS_PER_CHANNEL];
	end

	reg [8+SIZE_BITS:0] cnt_x = 0;  // time counter for BCM
	reg [3:0]           cnt_y = 0;  // row position
	reg [1:0]           cnt_z = 0;  // bit position of 4-bit color
	reg [8+SIZE_BITS:0] max_cnt_x;

	reg [4+SIZE_BITS:0] addr_x;
	reg [4:0]           addr_y;
	reg [2:0]           addr_z;

	reg [2:0] data_rgb;
	reg [2:0] data_rgb_q;

	reg state = 0;

    /* IMPLEMENTING BINARY CODED MODULATION */
	always @(posedge clk) begin
		case (cnt_z)
			0: max_cnt_x = 32*SIZE+8;
			1: max_cnt_x = 64*SIZE;
			2: max_cnt_x = 128*SIZE;
			3: max_cnt_x = 256*SIZE;
		endcase
	end

    /* IMPLEMENTING COUNTERS FOR BCM AND OTHER PURPOSES */
	always @(posedge clk) begin
		state <= !state;                   // doubled-period of clk signal
		if (!state) begin
			if (cnt_x > max_cnt_x) begin
				cnt_x <= 0;                // reset x
				cnt_z <= cnt_z + 1'd1;     // increment z which changes max for x
				if (cnt_z == BITS_PER_CHANNEL-1) begin
					cnt_y <= cnt_y + 1'd1;
					cnt_z <= 0;
				end
			end else begin
				cnt_x <= cnt_x + 1'd1;       // Counting x up every other clk tick
			end
		end
	end

    /* IMPLEMENTING PANEL CLOCK & LATCH */
	always @(posedge clk) begin
		panel_oe <= 32*SIZE-8-EXTRA_BLANKING < cnt_x && cnt_x < 32*SIZE+8; // act. low
		if (state) begin                         // when cnt_x is not incremented
			panel_clk <= 1 < cnt_x && cnt_x < 32*SIZE+2; // clock for shifting
			panel_stb <= cnt_x == 32*SIZE+2;     // latch during 1 clk tick
		end else begin
			panel_clk <= 0;
			panel_stb <= 0;
		end
	end

    /* RECYCLE COUNTERS TO ACQUIRE ADDRS */
	always @(posedge clk) begin
		addr_x <= cnt_x[4+SIZE_BITS:0];
		addr_y <= cnt_y + 5'd16*(!state);  // alternating addrs of the 2 halves
		addr_z <= cnt_z;
	end

    /* SEPARATE DATA FOR SHIFTING */
	always @(posedge clk) begin
		data_rgb[2] <= video_mem_r[{addr_y, addr_x}][addr_z];
		data_rgb[1] <= video_mem_g[{addr_y, addr_x}][addr_z];
		data_rgb[0] <= video_mem_b[{addr_y, addr_x}][addr_z];
	end

    /* SHIFT DATA */
	always @(posedge clk) begin
		data_rgb_q <= data_rgb;
		if (!state) begin
			if (0 < cnt_x && cnt_x < 32*SIZE+1) begin
				{panel_r1, panel_r0} <= {data_rgb[2], data_rgb_q[2]};
				{panel_g1, panel_g0} <= {data_rgb[1], data_rgb_q[1]};
				{panel_b1, panel_b0} <= {data_rgb[0], data_rgb_q[0]};
            end else begin
				{panel_r1, panel_r0} <= 0;
				{panel_g1, panel_g0} <= 0;
				{panel_b1, panel_b0} <= 0;
			end
		end else
		if (cnt_x == 32*SIZE - EXTRA_BLANKING/2) begin
			{panel_d, panel_c, panel_b, panel_a} <= cnt_y;
		end
	end
endmodule
