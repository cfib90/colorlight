/*******************************************************************************
** Project    : LED MATRIX
********************************************************************************
** File       : mod_top.v
** Author     : Kelve T. Henrique
** Company    : FH Technikum Wien
** Last update: 2018 Nov 19
** Platform   : Linux(Programming), ModelSim(Simul), Quartus II13.1(Synth)
********************************************************************************
** Description: Top level design
*******************************************************************************/
`default_nettype none
module mod_top(
    input wire osc25m,
    output wire led,

    output wire [3:0] R1,
    output wire [3:0] G1,
    output wire [3:0] B1,
    output wire [3:0] R2,
    output wire [3:0] G2,
    output wire [3:0] B2,
    output wire A,
    output wire B,
    output wire C,
    output wire D,
    output wire E, // a GND pin on the panels
    output wire LAT,
    output wire OE, //blank
    output wire CLK
);


    wire [3:0] w_en = 4'b0;
    wire [3:0] w_wr = 4'b0;
    wire [23:0] w_wdat = 24'b0;
    wire [15:0] w_addr = 16'b0;
    wire clk, lock, resetn;
    
    assign E = 1'b0; // a GND pin on the panels
    
    assign clk = osc25m;
    
    reg [3:0] resetn_gen;

    initial begin 
        resetn_gen <= 4'b0000;
    end
    assign resetn = resetn_gen[3];

    always @(posedge osc25m) begin
        resetn_gen <= {resetn_gen[2:0],1'b1};
    end

    //assign reset = 0;

    blinky INST_BLINKY(
        .osc25m(osc25m),
        .led(led)
    );

    mod_panel INST_PANEL0(
        .clk       (clk),
        .resetn    (resetn),
        .ctrl_en   (w_en[0]),
        .ctrl_wr   (w_wr),
        .ctrl_addr (w_addr),
        .ctrl_wdat (w_wdat),

        .ctrl_done (),       // connect to an FPGA's LED
	    .panel_r0  (R1[0]),
        .panel_g0  (G1[0]),
        .panel_b0  (B1[0]),
        .panel_r1  (R2[0]),
        .panel_g1  (G2[0]),
        .panel_b1  (B2[0]),
	    .panel_a   (A),
        .panel_b   (B),
        .panel_c   (C),
        .panel_d   (D),
        .panel_clk (CLK),
        .panel_stb (LAT),
        .panel_oe  (OE)
    );

    mod_panel INST_PANEL1(
        .clk       (clk),
        .resetn    (resetn),
        .ctrl_en   (w_en[1]),
        .ctrl_wr   (w_wr),
        .ctrl_addr (w_addr),
        .ctrl_wdat (w_wdat),

        .ctrl_done (),       // connect to an FPGA's LED
	    .panel_r0  (R1[1]),
        .panel_g0  (G1[1]),
        .panel_b0  (B1[1]),
        .panel_r1  (R2[1]),
        .panel_g1  (G2[1]),
        .panel_b1  (B2[1]),
	    .panel_a   (),
        .panel_b   (),
        .panel_c   (),
        .panel_d   (),
        .panel_clk (),
        .panel_stb (),
        .panel_oe  ()
    );

    mod_panel INST_PANEL2(
        .clk       (clk),
        .resetn    (resetn),
        .ctrl_en   (w_en[2]),
        .ctrl_wr   (w_wr),
        .ctrl_addr (w_addr),
        .ctrl_wdat (w_wdat),

        .ctrl_done (),       // connect to an FPGA's LED
	    .panel_r0  (R1[2]),
        .panel_g0  (G1[2]),
        .panel_b0  (B1[2]),
        .panel_r1  (R2[2]),
        .panel_g1  (G2[2]),
        .panel_b1  (B2[2]),
	    .panel_a   (),
        .panel_b   (),
        .panel_c   (),
        .panel_d   (),
        .panel_clk (),
        .panel_stb (),
        .panel_oe  ()
    );
    
    mod_panel INST_PANEL3(
        .clk       (clk),
        .resetn    (resetn),
        .ctrl_en   (w_en[3]),
        .ctrl_wr   (w_wr),
        .ctrl_addr (w_addr),
        .ctrl_wdat (w_wdat),

        .ctrl_done (),       // connect to an FPGA's LED
	    .panel_r0  (R1[3]),
        .panel_g0  (G1[3]),
        .panel_b0  (B1[3]),
        .panel_r1  (R2[3]),
        .panel_g1  (G2[3]),
        .panel_b1  (B2[3]),
	    .panel_a   (),
        .panel_b   (),
        .panel_c   (),
        .panel_d   (),
        .panel_clk (),
        .panel_stb (),
        .panel_oe  ()
    );
endmodule
