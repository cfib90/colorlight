/*******************************************************************************
** Project    : LED MATRIX
********************************************************************************
** File       : mod_spi.v
** Author     : Kelve T. Henrique
** Company    : FH Technikum Wien
** Last update: 2019 Jan 28
** Platform   : Linux(Programming), ModelSim(Simul), Quartus II13.1(Synth)
********************************************************************************
** Description: SPI module
*******************************************************************************/

module mod_spi(
    input resetn,
    input clk,
    input sck,
    input cs,
    input mosi,

    output miso,
    output reg [3:0]  en,
    output reg [3:0]  wr,
    output reg [23:0] wdat,
    output reg [15:0] addr
);
    
    assign miso = 0;
    
    /* Oversampling SPI sck with the FPGA clk */
    reg [2:0] sck_sampled;

    always @(posedge clk)
        sck_sampled <= {sck_sampled[1:0], sck};

    wire sck_posedge = (sck_sampled[2:1] == 2'b01);


    /* Oversampling SPI cs with the FPGA clk */
    reg [2:0] cs_sampled;

    always @(posedge clk)
        cs_sampled <= {cs_sampled[1:0], cs};

    wire cs_active = ~cs_sampled[1]; // active low


    /* Oversampling SPI mosi with the FPGA clk */
    reg [1:0] mosi_sampled;

    always @(posedge clk)
        mosi_sampled <= {mosi_sampled[0], mosi};

    wire mosi_data = mosi_sampled[1];

    
    /* Receiving data */
    reg [4:0] bit_cnt;
    reg [19:0] received_addr;
    reg [11:0]  received_pixel;
    reg spi_state;
    localparam ADDRESSING = 1'b0, PIXELING = 1'b1;

    always @(posedge clk)
    begin
      if(~cs_active)
      begin
        bit_cnt   <= 5'b00000;
        spi_state <= ADDRESSING;
      end
      else
          if(sck_posedge)
          begin
              case (spi_state)
                  ADDRESSING:
                  begin
                        bit_cnt       <= bit_cnt + 5'b00001;
                        received_addr <= {received_addr[18:0], mosi_data};
                        if (bit_cnt == 5'b10011)     // 20 addressing bits
                        begin
                            bit_cnt   <= 5'b00000;
                            spi_state <= PIXELING;
                        end
                        else
                            spi_state <= ADDRESSING;
                  end
                  PIXELING:
                  begin
                        bit_cnt        <= bit_cnt + 5'b00001;
                        received_pixel <= {received_pixel[10:0], mosi_data};
                        if (bit_cnt == 5'b01011)     // 12 colour bits
                            bit_cnt <= 5'b00000;
                  end
              endcase
          end
    end


    /* Signaling when addresses & pixels are fully received */
    reg addr_received;
    reg pixel_received;

    always @(posedge clk)
    begin
        addr_received      <= cs_active && sck_posedge && (spi_state == ADDRESSING) && (bit_cnt == 5'b10011);
        pixel_received     <= cs_active && sck_posedge && (spi_state == PIXELING)   && (bit_cnt == 5'b01011);
    end


    /* Process packets */
    reg inContinuous;
    always @(posedge clk)
    begin
        if (~resetn)
        begin
            inContinuous <= 0;
            en      <= 4'b0000;
            addr    <= 0;
            wr      <= 0;
            wdat    <= 0;
        end
        else
        begin
            if (addr_received)
            begin
                case (received_addr[17:16])      // Determine row of panels
                    0: en <= 4'b0100;
                    1: en <= 4'b1000;
                    2: en <= 4'b0001;
                    3: en <= 4'b0010;
                endcase
                addr <= {received_addr[15:11], received_addr[10:4]};
                wr   <= received_addr[3:0];
                inContinuous <= 1;
            end
            if (pixel_received && inContinuous)
            begin
                wdat <= {received_pixel[11:8], 4'b0000, received_pixel[7:4], 4'b0000, received_pixel[3:0], 4'b0000};
                inContinuous <= 0;
            end
            if (pixel_received && ~inContinuous)
            begin
                wdat <= {received_pixel[11:8], 4'b0000, received_pixel[7:4], 4'b0000, received_pixel[3:0], 4'b0000};
                if (addr == 12'b111111111111)
                    en <= {en[2:0], en[3]};
                addr <= addr + 12'b1;
            end
        end
    end

endmodule
