/*******************************************************************************
** Project    : LED MATRIX
********************************************************************************
** File       : mod_glock.v
** Author     : Kelve T. Henrique
** Company    : FH Technikum Wien
** Last update: 2018 Nov 09
** Platform   : Linux(Programming), ModelSim(Simul), Quartus II13.1(Synth)
********************************************************************************
** Description: Implementing the 'gated lock' functionality as it is not
**              natively supported by Cyclone III
*******************************************************************************/

module mod_glock (
//    input clk50,
    input clkpll,
    input lock,

    output reg resetn
);

localparam ALPHA = 3'b000, BRAVO = 3'b001, CHARLIE = 3'b010, DELTA = 3'b011, LOCK = 3'b100;

reg [2:0] posstate;

always @(posedge clkpll, negedge lock)
begin
    if (~lock)
    begin
        resetn   <= 1'b0;
        posstate <= ALPHA;
    end
    else
    begin
        case (posstate)
            ALPHA:
            begin
                resetn <= 1'b0;
                if (lock == 1'b1)
                    posstate <= BRAVO;
            end
            BRAVO:
            begin
                resetn <= 1'b0;
                if (lock == 1'b1)
                    posstate <= CHARLIE;
                else
                    posstate <= ALPHA;
            end
            CHARLIE:
            begin
                resetn <= 1'b0;
                if (lock == 1'b1)
                    posstate <= DELTA;
                else
                    posstate <= ALPHA;
            end
            DELTA:
            begin
                resetn <= 1'b0;
                if (lock == 1'b1)
                    posstate <= LOCK;
                else
                    posstate <= ALPHA;
            end
            LOCK:
            begin
                resetn <= 1'b1;
                if (lock == 1'b1)
                    posstate <= LOCK;
                else
                begin
                    resetn   <= 1'b0;
                    posstate <= ALPHA;
                end
            end
            default:
            begin
                resetn   <= 1'b0;
                posstate <= ALPHA;
            end
        endcase
    end
end
    
endmodule
