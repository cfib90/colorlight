/*
 *  Copyright 2020, Christian Fibich
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose
 *  with or without fee is hereby granted, provided that the above copyright notice
 *  and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 *  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 *  AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 *  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 *  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 *  OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 *  PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/socket.h>
#include <netinet/ip.h>
#include <assert.h>
#include <unistd.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <signal.h>

#define BASE_PORT 26208
#define BLOCK_SIZE 32
#define NUM_ROWS 32

#ifndef SINGLE
#define NUM_PANEL_ROWS 4

#define NUM_COLS 128
#define BLOCK_SIZE 32
#define STARTX 0
#define USEC 25000
#else
#define NUM_PANEL_ROWS 1
#define NUM_ROWS 32
#define NUM_COLS 32
#define STARTX 96
#define USEC 40000
#endif
int interrupted = 0;
void sigint_handler(int t) {
    interrupted = 1;
}
int main(int argc, char *argv[]) {
    long usec = USEC;
    if (argc >= 2) {
        usec = strtol(argv[1],NULL,0);
    }
    signal (SIGINT,sigint_handler);

    srand(time(NULL));
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);
    assert(sockfd >= 0);
    uint32_t colorbars_buffer [4][32*128];
    uint32_t colors[] = {0x222,0x00F,0x0F0,0x0FF,0xF00,0xF0F,0xFF0,0xFFF};
    
    for (int i = 0; i < 4; i++) {
        uint16_t c = 0;
        for (int y = 0; y < 32; y++) {
            for (int x = 0; x < 128; x++) {
                uint32_t addr = ((y & 0x1F) << 7) | (x & 0x7F);
                colorbars_buffer[i][y*128+x] =  htonl((addr << 16) | colors[(rand())%8]);
            }
        }
    }
    struct sockaddr_in addr = {.sin_family = AF_INET, .sin_port = 0};
    inet_pton(AF_INET,"192.168.1.50",&addr.sin_addr.s_addr);
    while(1) {
        //FILE *fp = fopen("img.rgb","r");
        for (int i = 0; i < NUM_PANEL_ROWS; i++) {
            uint16_t c = 0;
            for (int y = 0; y < NUM_ROWS; y++) {
                for (int x = STARTX; x < STARTX+NUM_COLS; x++) {
                    uint32_t addr = ((y & 0x1F) << 7) | (x & 0x7F);
                    uint8_t col[4];
                    int rv = fread(col,1,4,stdin);
                    if (rv != 4 || interrupted) memset(col,0,4);
                    uint32_t color = ((col[1]&0xF0) << 4) | (col[2]&0xF0) | ((col[3]&0xF0)>>4);
                    colorbars_buffer[i][y*NUM_COLS+x] =  htonl((addr << 16) | color);
                }
            }
        }
        //fclose(fp);
        
        for (int i = 0; i < NUM_PANEL_ROWS; i++) {
            for (int y = 0; y < NUM_ROWS; y++) {
                addr.sin_port = htons(BASE_PORT+(1<<i));
                for (int x = STARTX; x < STARTX+NUM_COLS; x+=BLOCK_SIZE) {
                    int rv = sendto(sockfd,&(colorbars_buffer[i][y*NUM_COLS+x]),BLOCK_SIZE*4,0,(struct sockaddr *)(&addr),sizeof(addr));
                }
            }
        }
        if(feof(stdin)) break;
        usleep(usec);
    }
    
    close(sockfd);
}
