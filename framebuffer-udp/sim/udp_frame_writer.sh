set -e

iverilog ../mod_panel.v ../udp_panel_writer.v ../tb/udp_panel_writer_tb.v -o udp_panel_writer.exe
vvp udp_panel_writer.exe
