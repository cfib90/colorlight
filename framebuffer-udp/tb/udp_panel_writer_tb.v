module udp_panelwriter_tb();


    reg clock = 1'b0;
    reg display_clock = 1'b0;
    reg reset = 1'b1;
    
    wire [0:0] R0;
    wire [0:0] G0;
    wire [0:0] B0;
    wire [0:0] R1;
    wire [0:0] G1;
    wire [0:0] B1;
    wire A;
    wire B;
    wire C;
    wire D;
    wire CLK;
    wire LAT;
    wire OE;
    wire led;

    wire [3:0]  ctrl_en;
    wire [3:0]  ctrl_wr;
    wire [15:0] ctrl_addr;
    wire [23:0] ctrl_wdat;
    
    
    reg          udp_source_valid = 1'b0;
    reg          udp_source_last  = 1'b0;
    wire         udp_source_ready ;
    reg  [15:0]  udp_source_src_port = 1'b0;
    reg  [15:0]  udp_source_dst_port = 1'b0;
    reg  [31:0]  udp_source_ip_address = 1'b0;
    reg  [15:0]  udp_source_length = 1'b0;
    reg  [31:0]  udp_source_data = 1'b0;
    reg  [3:0]   udp_source_error = 1'b0;
    
    integer in;
    integer rv;
    
    
    initial begin
        $dumpfile("udp_panel_writer.vcd");
        in = $fopen("frames.txt","r");
        $dumpvars();
        repeat (10) @(posedge clock);
        reset <= 1'b0;
        repeat (1000) @(posedge clock);
        @(negedge clock);
        while (!$feof(in)) begin
            rv = $fscanf(in,"FRAME srcport=%d dstport=%d ip=%h length=%d\n",udp_source_src_port,udp_source_dst_port,udp_source_ip_address,udp_source_length);
            if (rv != 4) begin
                rv = $fscanf(in,"WORD data=%h last=%b\n",udp_source_data,udp_source_last);
                if (rv == 2) begin
                    while (!udp_source_ready) begin
                        @(negedge clock);
                    end
                    udp_source_valid <= 1'b1;
                    @(negedge clock);
                    if (udp_source_last)
                        udp_source_valid <= 1'b0;
                    udp_source_valid <= 1'b0;
                    repeat (10) @(negedge clock);
                end else begin
                    $display("ERROR: Invalid frames.txt");
                    $finish();
                end
            end
        end
        repeat (1000) @(posedge clock);
        $finish();
    end
    
    
    udp_panel_writer dut (.clock(clock),
                     .reset(reset),
                     
                     .udp_source_valid(udp_source_valid),
                     .udp_source_last(udp_source_last),
                     .udp_source_ready(udp_source_ready),
                     .udp_source_src_port(udp_source_src_port),
                     .udp_source_dst_port(udp_source_dst_port),
                     .udp_source_ip_address(udp_source_ip_address),
                     .udp_source_length(udp_source_length),
                     .udp_source_data(udp_source_data),
                     .udp_source_error(udp_source_error),

                     .ctrl_en(ctrl_en),
                     .ctrl_wr(ctrl_wr),
                     .ctrl_addr(ctrl_addr),
                     .ctrl_wdat(ctrl_wdat),
                     .led_reg(led)
                     );

    mod_panel panel_0 (
        .ctrl_clk(clock),
        .ctrl_en(ctrl_en[0]),
        .ctrl_wr(ctrl_wr),       // Which color memory block to write
        .ctrl_addr(ctrl_addr),   // Addr to write color info on [col_info][row_info]
        .ctrl_wdat(ctrl_wdat),   // Data to be written [R][G][B]

        .display_clock(display_clock),
        .panel_r0(R0[0]),
        .panel_g0(G0[0]),
        .panel_b0(B0[0]),
        .panel_r1(R1[0]),
        .panel_g1(G1[0]),
        .panel_b1(B1[0]),
        .panel_a(A),
        .panel_b(B),
        .panel_c(C),
        .panel_d(D),
        .panel_clk(CLK),
        .panel_stb(LAT),
        .panel_oe(OE)
    );
    
    
    always
        #4 clock <= ~clock;
        
    always
        #20 display_clock <= ~display_clock;

endmodule
